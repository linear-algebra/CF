
## 本科-公司金融-课程大纲-连玉君


&emsp;

## ${\color{red}{News}}$ &#x1F332;



&emsp;

》》 过往
> - &#x1F449; 个人案例Hola-Kola的提交截止时间：**2021年xx月xx日23:59**； `更新时间：xx `
> - &#x1F449; 案例A1贵州茅台、A2哔哩哔哩的小组案例材料提交截止日期：**2021年xx月xx日23:59**。`更新时间：xx `
> - 其他小组案例的材料提交时间和展示时间已更新。`更新时间：2021/xxx`   
> - 个人案例作业和小组案例思考题已发布 [点击查看](https://gitee.com/arlionn/CF/wikis/CF%20%E6%A1%88%E4%BE%8B%E5%88%86%E6%9E%90%E6%8A%A5%E5%91%8A%E8%A6%81%E6%B1%82.md?sort_id=1632641) `2020/10/3 12:15`   
> - 小组案例报告资料参见第 【4.2 案例资料和要求】小节

> - 第一次作业已发布 `2021/xxx`   

&emsp;

## 1. 课程概览


- **[课程主页](https://gitee.com/arlionn/CF)：** <https://gitee.com/arlionn/CF> &#x1F34F;
- **任课教师：** 连玉君，arlionn@163.com , Tel：020-84110648
- **助教：** 刘畅 博士 (<lc_zd@163.com>)
- **成绩构成：** 
  - 课堂参与和考勤 10%；课堂参与（小组案例报告）15%；个人作业 15%；期末考试 60%
- **课程内容和进度**：以教务系统公布版本为准。本文底部可以快速浏览。
- **教材：** 乔纳森•伯克、彼得•德马佐著，《公司理财》（上下册，第三版） 姜英兵译，中国人民大学出版社，ISBN：9787300196312, ([上](https://quqi.gblhgk.com/s/880197/1YUQk8Hr9iseBcJ5)，[下](https://quqi.gblhgk.com/s/880197/yAFra4o9dPfSjuzO))，可点击下载电子版。
- **课件下载：** <ftp://ftp.lingnan.sysu.edu.cn/>，账号=密码=lianyjst

&emsp; 


--- - --

## 2. &#x1F353; 交作业入口
> &#x1F449; **点击提交：** 
> 
> A. [第二次作业](https://workspace.jianguoyun.com/inbox/collect/8762fbb45fa04bf797e2ffbaf31f5bed/submit) &emsp;  [第三次作业](https://workspace.jianguoyun.com/inbox/collect/e2beaeff2b964223be9934dce59d71d0/submit) &emsp; 
> 
> B. [小组案例报告](https://workspace.jianguoyun.com/inbox/collect/5823f62f1ed3469e86015cae5fe6f5f0/submit) 

> Notes：
> - [1] 只需提交电子版即可；若需更新，只需将同名文件再次提交即可。
> - [2] 提交截止时间为当日 23:59，逾期不候。

> 关于Markdown文件中图片的说明
> - 如果提交的Markdown(.md)文件中包含图片，请注意检查图片在其他设备上是否可以正常打开。
> - 使用的图片链接为本地文件时，其他用户将无法查看该图片。可以另存为pdf格式后上传，或使用互联网图床添加图片。


---



&emsp;

## 3. 个人作业


**教材：** 乔纳森•伯克、彼得•德马佐著，《公司理财》（上下册，第三版） 姜英兵译，中国人民大学出版社，ISBN：9787300196312。

> 请前往 Wiki [「公司金融作业」](https://gitee.com/arlionn/CF/wikis/Home) 页面查看。
> - **HW01：** 截止时间：2021年9月12日 ==待更新==
> - **HW02：** 截止时间：视教学进度待定
> - **HW03：** 截止时间：视教学进度待定
> - **Hola-Kola：** 个人案例作业：截止时间：2021年xx月xx日
> - **HW04：** 截止时间：视教学进度待定
> - **HW05：** 截止时间：视教学进度待定

**个人案例分析**
- &#x1F449; Hola-Kola (NPV)，[A. 点击下载案例材料](https://www.jianguoyun.com/p/DQbIcpQQtKiFCBj4kr0D)；[B. 案例思考题](https://gitee.com/arlionn/CF/wikis/CF%20%E6%A1%88%E4%BE%8B%E5%88%86%E6%9E%90%E6%8A%A5%E5%91%8A%E8%A6%81%E6%B1%82.md?sort_id=1632641)
    - [备用链接](https://ranglab-pic.oss-cn-shenzhen.aliyuncs.com/Hola-Kola.zip)

&emsp;

## 4. 小组案例分析报告

### 4.1 规则
  - 全班同学分成 10 个小组
  - 每个小组选择一个案例，完成案例分析报告
  - 老师确定时间展示，每小组 20-30 分钟

### 4.2 案例资料和要求
> **A.** 案例材料下载：[-点击下载-](https://www.jianguoyun.com/p/DWTFIYYQtKiFCBj8kr0D)（[备用链接](https://ranglab-pic.oss-cn-shenzhen.aliyuncs.com/%E5%B0%8F%E7%BB%84%E6%A1%88%E4%BE%8B.zip)）。
> **B.** 案例思考题和案例报告要求：[-点击查看-](https://gitee.com/arlionn/CF/wikis/CF%20%E6%A1%88%E4%BE%8B%E5%88%86%E6%9E%90%E6%8A%A5%E5%91%8A%E8%A6%81%E6%B1%82.md?sort_id=1632641)  
> **C.** 案例报告提交：参见【2. &#x1F353; 交作业入口】。   
> **D.** 提交截止时间：相应案例课堂展示前一天。&#x1F449; [2020年秋季-中山大学校历](https://www.jianguoyun.com/p/DY5Qf_EQtKiFCBi-pb0D)

### 4.3 案例主题
- A. 估值专题：A1. 贵州茅台；A2. 哔哩哔哩 
- B. 融资专题：B1. 开心麻花；B2. 泰禾集团
- C. 股利专题：C1. 苹果股利；C2. 德艺文创
- D. 公司治理：D1. 阿里合伙人；D2. 万科之争
- E. 兼并收购：E1. 艾派克蛇吞象；E2. 银河电子

> 分组情况：   

==待更新==


&emsp;


## 5. 期末考试

- 考试时间：Week 20
- 考试形式：闭卷
- **要求和说明：**
  - 可以带一张 A4 小抄，正反面均可记录你认为有价值的信息；
  - 可以带计算器（包括简易计算器和金融计算器）；
  - 请将手机调至静音或关闭状态，考试期间不能使用手机；



  
&emsp;

## 6. 教材和参考资料

### 6.1 教材

> 推荐顺序：Berk &rarr; 达摩达兰 &rarr; Asquith

- &#x1F34E;  Berk, J., P. DeMarzo. Corporate Finance. Prentice Hall, 2006. [-英文版\-](https://www.jianguoyun.com/p/Dee2xg4QtKiFCBjxsqoD)，[-中文上\-](https://www.jianguoyun.com/p/DUdv-BQQtKiFCBjysqoD)，[-中文下\-](https://www.jianguoyun.com/p/DTqNuMMQtKiFCBjzsqoD)
- 达摩达兰-2015-4th-中文-应用公司理财，[-PDF-](https://www.jianguoyun.com/p/DaFA6N4QtKiFCBjIqqoD), [-英文版-](https://quqi.gblhgk.com/s/880197/YNTMFtc3BQRJWaTX)
- 保罗·阿斯奎思, 劳. A.韦斯. 公司金融:金融工具、财务政策和估值方法的案例实践[M]. 机械工业出版社, 2018. [-PDF\-](https://www.jianguoyun.com/p/DejvIz4QtKiFCBjlsqoD). 书中的案例都很经典，分析的极为透彻。这些案例已经在 MIT 的 MBA 课堂上讨论了好多年。 [-英文版\-](https://www.jianguoyun.com/p/DcKNlN8QtKiFCBjqsqoD)
- Myers, Principle of Corporate Finance, 12th-E, [-PDF\-](https://www.jianguoyun.com/p/DYATTqEQtKiFCBimkqsD)

### 6.2 其他参考资料

- **估值** 达摩达兰, 投资估价：评估任何资产价值的工具和技术，2014，第 3 版，清华大学出版社. [中文-上](https://www.jianguoyun.com/p/Dcs4oboQtKiFCBjwkKsD)，[英文](https://www.jianguoyun.com/p/DZvsKpoQtKiFCBj2kKsD)。估值领域的经典之作。
- Holden-2015-**Excel Modeling** in Corporate Finance 5e， [-PDF-](https://quqi.gblhgk.com/s/880197/6JBMSQP2ipMAq8KR)，[-配套 Excel-xlsx-](https://quqi.gblhgk.com/s/880197/VsPpOXVU5aum2A5q)，[-配套 Excel-xls-](https://quqi.gblhgk.com/s/880197/XDngKwIjIa9q3i5x)
- 马永斌. 公司治理之道:控制权争夺与股权激励 (第二版)[M]. 清华大学出版社, 2018. [-Link-](https://item.jd.com/12466396.html)，公司治理，通俗易懂，提供了很多例子

<div STYLE="page-break-after: always;"></div>

&emsp;

## 7. 附：课程内容和进度

&emsp;

> **Note：** 周次以校历日期为准：[2020年秋季-中山大学校历](https://www.jianguoyun.com/p/DY5Qf_EQtKiFCBi-pb0D)

周次 | 主题  | 学时  | 章节
---|:---|:---|:---|
**W1**	| 公司财务简介 |	2	| 前言
**W1**	| 公司组织形式和所有权结构 |	2	| 第1-2章
W2	| 财务决策与一价定律 |	2	| 第3章
W2	| 货币时间价值 |	2	| 第4章
**W3**	| 利率理论 |	2	| 第5章
**W3**	| 债券估值 |	2	| 第6章
W4	| 投资决策法则 |	2	| 第7章
W4	| 资本预算的基本原理 |	2	| 第8章
**W5**	| 股票估值 |	2	| 第9章
**W5**	| 资本市场与风险的定价 |	2	| 第10章
W6 |	最优投资组合选择 |	2	| 第11章
W6 |	资本资产定价模型 |	2	| 第12章
**W7** |	个人案例和小组案例讨论 |	2	| 个人：P0-Hola-Cola; <br>小组：A1-贵州茅台; A2-哔哩哔哩
**W7** |	完美市场中的资本结构 |	2	| 第14章
W8 |	债务和税收 |	2	| 第15章
W8 |	财务困境、管理者激励与信息 |	2	| 第16章
**W9** |	公司融资方式 |	2	| 第 23-24 章
**W9** |	股利政策 |	2	| 第17章
W11 |	小组案例讨论 |	2	| B1-开心麻花；B2-泰禾集团
W12 |	小组案例讨论 |	2	| C1-苹果股利；C2-德艺文创
W13 |	有杠杆时的资本预算与估值 |	2	| 第18章
W14 |	估值与财务建模: **案例分析展示 Case1** |	2	| 第19章
W15 |	公司治理-理论 |	2	| 第29章
W16 |	小组案例讨论 |	2	| D1-阿里合伙人；D2-万科之争
W17 |	兼并与收购：理论 |	2	| 第28章
W18 |	小组案例讨论 |	2	| E1-艾派克蛇吞象；E2-银河电子
W19 |	复习 |	2	| 课程复习；答疑
W20 |	期末考试 |	0	| 闭卷考试

> **温馨提示：** 第 1-9 周，每周两次课；第 11-19 周，每周一次课。  
